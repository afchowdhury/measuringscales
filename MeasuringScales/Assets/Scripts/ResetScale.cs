﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetScale : MonoBehaviour
{
    
    public Text[] quantities;

    public List<GameObject> spawnedObjects;

    public AddShape addShape;
    
    public GameObject scaleHandle;

    public void Start()
    {
        spawnedObjects = new List<GameObject>();
    }
    
    public void ResetIt(){
        
        foreach(GameObject x in spawnedObjects)
        {
            Destroy(x);
        }

        addShape.CircleList.Clear();
        addShape.HexagonList.Clear();
        addShape.SquareList.Clear();
        addShape.TriangleList.Clear();
        addShape.OnekgList.Clear();
        addShape.TwokgList.Clear();

        addShape.quantity10g.text = addShape.CircleList.Count.ToString();
        addShape.quantity5g.text = addShape.HexagonList.Count.ToString();
        addShape.quantity50g.text = addShape.SquareList.Count.ToString();
        addShape.quantity100g.text = addShape.TriangleList.Count.ToString();
        addShape.quantity1g.text = addShape.OnekgList.Count.ToString();
        addShape.quantity2g.text = addShape.TwokgList.Count.ToString();
        
        // addShape.weightOfShapes = 0;
        // addShape.UpdateText();
        spawnedObjects = new List<GameObject>();
        addShape.ResetHandle();
        addShape.scaleHandle.GetComponent<Transform>();
        addShape.weightOfShapes = 0;
        addShape.UpdateText();
        
        

    }
    
}
