﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddShape : MonoBehaviour
{

    public GameObject[] shapesObj; 

    public Transform[] spawnPoints;

    public GameObject[] weighingScaleVariations;

    public GameObject scaleHandle;

    public int weightOfShapes;

    public int maximumWeight;

    public float angleOfRotation, degPerKilo,scaleModify;

    public Text quantity1g;

    public Text quantity2g;

    public Text quantity5g;

    public Text quantity10g;

    public Text quantity50g;

    public Text quantity100g;

    public Text weightText;

    public List<GameObject> CircleList = new List<GameObject>();
    public List<GameObject> HexagonList = new List<GameObject>();
    public List<GameObject> SquareList = new List<GameObject>();
    public List<GameObject> TriangleList = new List<GameObject>();
    public List<GameObject> OnekgList = new List<GameObject>();
    public List<GameObject> TwokgList = new List<GameObject>();

    public List<Transform> editableSpawnList;
    public Transform spawnPosition;
    public int randomNumber;

    public ResetScale resetScale;

    // Start is called before the first frame update
    void Start()
    {
        foreach(Transform i in spawnPoints)
        {
            editableSpawnList.Add(i);
        }
        
        ResetHandle();
        scaleHandle.GetComponent<Transform>();
        MaximumWeight(1);
        UpdateText();
        ChangeFace(1);
    }

    public void ResetHandle()
    {
        scaleHandle.GetComponent<Transform>().transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    //Assigning a public Rigidbody component in the inspector to instantiate

    // Update is called once per frame
    void Update()
    {
        

    }


    // Circle, Hexagon, Square, Triangle 
   // public void SpawnShape (int whichShape){
    //     GameObject clone;
    //     clone = Instantiate(shapesObj[whichShape],spawnPosition]);

    //         switch (whichShape)

    // {
    
    //     case 0:
    //         weightOfShapes += 10;
    //         CircleList.Add(clone);
    //     break;
    //     case 1:
    //         weightOfShapes += 5;
    //         HexagonList.Add(clone);
    //     break;
    //     case 2:
    //         weightOfShapes += 50;
    //         SquareList.Add(clone);
    //     break;
    //     case 3:
    //         weightOfShapes += 100;
    //         TriangleList.Add(clone);
    //     break;

    // }

   

    
            // Circle, Hexagon, Square, Triangle 
    public void SpawnShape (int whichShape){
        if(editableSpawnList.Count > 1)
        {
            randomNumber = Random.Range(0,editableSpawnList.Count);
            
            spawnPosition = editableSpawnList[randomNumber].transform;

            editableSpawnList.RemoveAt(randomNumber);
        }
        else
        {
            spawnPosition = editableSpawnList[0].transform;
            editableSpawnList.RemoveAt(0);

            foreach(Transform i in spawnPoints)
            {
                editableSpawnList.Add(i);
            }
        }
        
        if(weightOfShapes<maximumWeight)
    {
        GameObject clone;

            switch (whichShape)

    {
    
        case 0:
        if(maximumWeight > weightOfShapes + 9){
            
        clone = Instantiate(shapesObj[whichShape],spawnPosition);
            weightOfShapes += 10;
            CircleList.Add(clone);
            quantity10g.text = CircleList.Count.ToString();
            resetScale.spawnedObjects.Add(clone);
        }
        break;
        case 1:
        if(maximumWeight > weightOfShapes + 4){
       
        clone = Instantiate(shapesObj[whichShape],spawnPosition);
            weightOfShapes += 5;
            HexagonList.Add(clone);
            quantity5g.text = HexagonList.Count.ToString();
            resetScale.spawnedObjects.Add(clone);

        }
        break;
        case 2:
        if(maximumWeight > weightOfShapes + 49){
        
        clone = Instantiate(shapesObj[whichShape],spawnPosition);
            weightOfShapes += 50;
            SquareList.Add(clone);
            quantity50g.text = SquareList.Count.ToString();
            resetScale.spawnedObjects.Add(clone);
        }
        break;
        case 3:
        if(maximumWeight > weightOfShapes + 99){
        
        clone = Instantiate(shapesObj[whichShape],spawnPosition);
            weightOfShapes += 100;
            TriangleList.Add(clone);
            quantity100g.text = TriangleList.Count.ToString();
            resetScale.spawnedObjects.Add(clone);
        }
        break;
        case 4:
        if(maximumWeight > weightOfShapes + 0){
        
        clone = Instantiate(shapesObj[whichShape],spawnPosition);
            weightOfShapes += 1;
            OnekgList.Add(clone);
            quantity1g.text = OnekgList.Count.ToString();
            resetScale.spawnedObjects.Add(clone);
        }
        break;
        case 5:
        if(maximumWeight > weightOfShapes + 1){
        
        clone = Instantiate(shapesObj[whichShape],spawnPosition);
            weightOfShapes += 2;
            TwokgList.Add(clone);
            quantity2g.text = TwokgList.Count.ToString();
            resetScale.spawnedObjects.Add(clone);
        }
        break;

    }

    
     UpdateText();

    }

   
    }

       public void RemoveShape (int whichShape){


            switch (whichShape)

    {
    
        case 0:
            if(CircleList.Count > 0)
            {
                weightOfShapes -= 10;
                Destroy(CircleList[CircleList.Count-1]);
                CircleList.Remove(CircleList[CircleList.Count-1]);
                quantity10g.text = CircleList.Count.ToString();
            }
            
        break;
        case 1:
            if(HexagonList.Count > 0)
            {
                weightOfShapes -= 5;
                Destroy(HexagonList[HexagonList.Count-1]);
                HexagonList.Remove(HexagonList[HexagonList.Count-1]);
                quantity5g.text = HexagonList.Count.ToString();
            }
            
        break;
        case 2:
            if(SquareList.Count > 0)
            {
                weightOfShapes -= 50;
                Destroy(SquareList[SquareList.Count-1]);
                SquareList.Remove(SquareList[SquareList.Count-1]);
                quantity50g.text = SquareList.Count.ToString();
            }
            
        break;
        case 3:
            if(TriangleList.Count > 0)
            {
                weightOfShapes -= 100;
                Destroy(TriangleList[TriangleList.Count-1]);
                TriangleList.Remove(TriangleList[TriangleList.Count-1]);
                quantity100g.text = TriangleList.Count.ToString();
            }
            
        break;
        case 4:
            if(OnekgList.Count > 0)
            {
                weightOfShapes -= 1;
                Destroy(OnekgList[OnekgList.Count-1]);
                OnekgList.Remove(OnekgList[OnekgList.Count-1]);
                quantity1g.text = OnekgList.Count.ToString();
            }
            
        break;
        case 5:
            if(TwokgList.Count > 0)
            {
                weightOfShapes -= 2;
                Destroy(TwokgList[TwokgList.Count-1]);
                TwokgList.Remove(TwokgList[TwokgList.Count-1]);
                quantity2g.text = TwokgList.Count.ToString();
            }
            
        break;


    }

    UpdateText();

   
    }

    public void UpdateText()
    {
        weightText.text = "Total Weight: " + weightOfShapes.ToString();
        PercentileDifference();

    }



    //switch statement to individually turn on each weighing scale variations
    public void ChangeFace (int chooseFace)
    {
        foreach (var item in weighingScaleVariations)
        {
            item.SetActive(false);
        }

         weighingScaleVariations[chooseFace].SetActive(true);
    }



    public void MaximumWeight (int maxWeight)
    {
        
        switch (maxWeight)

        {
        case 0:
            maximumWeight = 10;
            scaleModify = 1f;
        break;
        case 1:
            maximumWeight = 100;
            scaleModify = 10f;
        break;
        case 2:
            maximumWeight = 200;
            scaleModify = 20f;
        break;
        case 3:
            maximumWeight = 500;
            scaleModify = 50f;
        break;
        case 4:
            maximumWeight = 1000;
            scaleModify = 100f;
        break;
        }

        foreach ( var item in HexagonList){
            Destroy(item);
        }

        foreach ( var item in CircleList){
            Destroy(item);
        }

        foreach ( var item in SquareList){
            Destroy(item);
        }

        foreach ( var item in TriangleList){
            Destroy(item);
        }

        foreach ( var item in OnekgList){
            Destroy(item);
        }

        foreach ( var item in TwokgList){
            Destroy(item);
        }
        
        weightOfShapes = 0;
        UpdateText();
    }

    public void PercentileDifference()
    {
        degPerKilo = 360f/(11f*scaleModify);
        angleOfRotation = degPerKilo * weightOfShapes;
        scaleHandle.GetComponent<Transform>().transform.rotation = Quaternion.Euler(0, 0, -angleOfRotation);
    }

}
