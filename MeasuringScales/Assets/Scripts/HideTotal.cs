﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideTotal : MonoBehaviour
{
    public Text buttonText;

    public bool textVisible;

    public GameObject scoreBoard;

    public AddShape addShape;
    
    // Start is called before the first frame update
    void Start()
    {
        textVisible = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

        public void HideTotalWeight()
    {
        if(textVisible)
        {
            scoreBoard.SetActive(false);

            textVisible = false;

            buttonText.text = "Show Total";

        }
        
        else
        {
            scoreBoard.SetActive(true);
            
            textVisible = true;

            buttonText.text = "Hide Total";

            //addShape.UpdateText();
            
        }

    }

}
